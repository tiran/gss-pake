/*
 * Copyright (c) 2022 Andreas Schneider <asn@redhat.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived from
 *      this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <gssapi/gssapi.h>

#include "torture-cmocka.h"
#include "gss-pake.h"

#define SPNEGO_OID_STR "\053\006\001\005\005\002"
#define SPNEGO_OID_LEN 6
static gss_OID_desc mech_spnego = {
    .length = SPNEGO_OID_LEN,
    .elements = discard_const(SPNEGO_OID_STR),
};

struct gss_status_struct {
    const char *name;
    uint32_t code;
};

#define GSS_STATUS_ELEMENT(x) \
    { .code = (x), .name = #x }
#define GSS_STATUS_ELEMENT_NULL() \
    { .code = 0, .name = NULL }

static const struct gss_status_struct gss_status_table[] = {
    GSS_STATUS_ELEMENT(GSS_S_COMPLETE),
    GSS_STATUS_ELEMENT(GSS_S_BAD_MECH),
    GSS_STATUS_ELEMENT(GSS_S_BAD_NAME),
    GSS_STATUS_ELEMENT(GSS_S_BAD_NAMETYPE),
    GSS_STATUS_ELEMENT(GSS_S_BAD_BINDINGS),
    GSS_STATUS_ELEMENT(GSS_S_BAD_STATUS),
    GSS_STATUS_ELEMENT(GSS_S_BAD_SIG),
    GSS_STATUS_ELEMENT(GSS_S_BAD_MIC),
    GSS_STATUS_ELEMENT(GSS_S_NO_CRED),
    GSS_STATUS_ELEMENT(GSS_S_NO_CONTEXT),
    GSS_STATUS_ELEMENT_NULL(),
};

static const char *get_gss_status_str(uint32_t major)
{
    static char msg[32];
    size_t idx;

    for (idx = 0; gss_status_table[idx].name != NULL; idx++) {
        if (gss_status_table[idx].code == major) {
            return gss_status_table[idx].name;
        }
    }

    snprintf(msg, sizeof(msg), "GSS_STATUS(%#04x)", major);

    return msg;
}

static void display_status(const char *msg, uint32_t status, int32_t type)
{
    OM_uint32 min_stat, msg_ctx = 0;
    gss_buffer_desc buf;

    do {
        (void)gss_display_status(&min_stat,
                                 status,
                                 type,
                                 &mech_spnego,
                                 &msg_ctx,
                                 &buf);
        fprintf(stderr,
                "%s: %.*s\n",
                msg,
                (int)buf.length,
                (char *)buf.value);

        (void)gss_release_buffer(&min_stat, &buf);
    } while (msg_ctx != 0);
}

void _assert_gss_status(uint32_t major,
                        uint32_t minor,
                        const char *function,
                        const char *filename,
                        const uint32_t line)
{
    if (GSS_ERROR(major)) {
        display_status(function, major, GSS_C_GSS_CODE);
        display_status(function, minor, GSS_C_MECH_CODE);
        print_error("GSS status major: %s, minor: %#04x\n",
                 get_gss_status_str(major),
                 minor);
        _fail(filename, line);
    }
}
