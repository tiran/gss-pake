/*
 * Copyright (c) 2022 Andreas Schneider <asn@redhat.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the copyright holder nor the names of its
 *      contributors may be used to endorse or promote products derived from
 *      this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <gssapi/gssapi.h>
#include <stdlib.h>

#include "torture-cmocka.h"

#define SPNEGO_OID_STR "\053\006\001\005\005\002"
#define SPNEGO_OID_LEN 6
gss_OID_desc mech_spnego = {
    .length = SPNEGO_OID_LEN,
    .elements = discard_const(SPNEGO_OID_STR),
};

static int setup(void **state)
{
    int rc;

    rc = setenv(
        "GSS_PAKE_PSK",
        "31b186ad36e251b72f9fad457b892dbfff46dbdb92a3ca664e2c768f05d5df41",
        1);
    assert_return_code(rc, errno);

    return 0;
}

static void test_gss_sec_context_init(void **state)
{
    OM_uint32 major, minor = 0;
    OM_uint32 flags = GSS_C_REPLAY_FLAG | GSS_C_SEQUENCE_FLAG;
    gss_ctx_id_t ictx = GSS_C_NO_CONTEXT;
    gss_buffer_desc itok = {
        .length = 0,
    };
    gss_buffer_desc atok = {
        .length = 0,
    };
    gss_buffer_desc buf = {
        .length = 4,
        .value = discard_const("host"),
    };
    gss_name_t name;

    major = gss_import_name(&minor, &buf, GSS_C_NT_HOSTBASED_SERVICE, &name);
    assert_gss_status(major, minor);

    major = gss_init_sec_context(&minor,
                                 GSS_C_NO_CREDENTIAL,
                                 &ictx,
                                 name,
                                 &mech_spnego,
                                 flags,
                                 GSS_C_INDEFINITE,
                                 NULL,
                                 &atok,
                                 NULL,
                                 &itok,
                                 NULL,
                                 NULL);
    assert_gss_status(major, minor);
    assert_non_null(ictx);
}

int main(int argc, char *argv[])
{
    int rc;

    const struct CMUnitTest display_status_tests[] = {
        cmocka_unit_test_setup_teardown(test_gss_sec_context_init, setup, NULL),
    };

    rc = cmocka_run_group_tests(display_status_tests, NULL, NULL);

    return rc;
}
